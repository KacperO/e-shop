using System.Linq;
using System.Threading.Tasks;
using Core.Entities.Identity;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Identity
{
    public class AppIdentityDbContextSeed
    {
        public static async Task SeedUser(UserManager<AppUser> manager) 
        {
            if (!manager.Users.Any()) 
            {
                var user = new AppUser 
                {
                    DisplayName = "Kacper",
                    Email = "kacper@test.com",
                    UserName = "kacper@test.com",
                    Address = new Address
                    {
                        FirstName = "Kacper",
                        LastName = "Kowalski",
                        Street = "Spokojna",
                        City = "Paterek",
                        State = "Poland",
                        ZipCode = "89100"
                    }
                };

                await manager.CreateAsync(user, "Pa$$w0rd");
            }
        }
    }
}